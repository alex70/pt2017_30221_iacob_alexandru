package polinom;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c1 = new GridBagConstraints();
	private JButton button = new JButton("Adunare");
	private JTextField text = new JTextField(30);
	private JLabel label = new JLabel("Adunare:");
	GridBagConstraints c2 = new GridBagConstraints();
	private JButton button2 = new JButton("Scadere");
	private JTextField text2 = new JTextField(30);
	private JLabel label2 = new JLabel("Scadere:");
	GridBagConstraints c3 = new GridBagConstraints();
	private JButton button3 = new JButton("Derivare");
	private JLabel label3 = new JLabel("Derivare:");
	GridBagConstraints c4 = new GridBagConstraints();
	private JButton button4 = new JButton("Integrare");
	private JLabel label4 = new JLabel("Integrare:");
	GridBagConstraints c5 = new GridBagConstraints();
	private JButton button5 = new JButton("Inmultire");
	private JLabel label5 = new JLabel("Inmultire:");
	public GUI(String name) {
		super(name);
		c1.gridx = 0;
		c1.gridy = 2;
		pane.add(button, c1);
		button.addActionListener(this);
		c1.gridx = 0;
		c1.gridy = 0;
		pane.add(text, c1);
		c1.gridx = 1;
		c1.gridy = 2;
		pane.add(label, c1);
		this.add(pane);
		
		c2.gridx = 0;
		c2.gridy = 3;
		pane.add(button2, c2);
		button2.addActionListener(this);
		c2.gridx = 0;
		c2.gridy = 1;
		pane.add(text2,c2);
		c2.gridx = 1;
		c2.gridy = 3;
		pane.add(label2, c2);
		
		c3.gridx = 0;
		c3.gridy = 4;
		pane.add(button3, c3);
		button3.addActionListener(this);
		c3.gridx = 1;
		c3.gridy = 4;
		pane.add(label3, c3);
		
		c4.gridx = 0;
		c4.gridy = 5;
		pane.add(button4, c4);
		button4.addActionListener(this);
		c4.gridx = 1;
		c4.gridy = 5;
		pane.add(label4, c4);
		
		c5.gridx = 0;
		c5.gridy = 6;
		pane.add(button5, c5);
		button5.addActionListener(this);
		c5.gridx = 1;
		c5.gridy = 6;
		pane.add(label5, c5);
	}
	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
			String s = text.getText();
			String s2 = text2.getText();
		if(source == button){
			Polinom p = new Polinom(s);
			Polinom n = new Polinom(s2);
			Polinom ad=new Polinom();
			ad=ad.adunare(p, n);
			String adn="0";
			for(int i=0;i<ad.monoame.size();i++)
			{
				if(i==0)
					adn=Float.toString(ad.monoame.get(i).getCoeff()) + "x^" + Integer.toString(ad.monoame.get(i).getExp())+"+";
				else
					if(i==ad.monoame.size()-1)
						adn=adn+Float.toString(ad.monoame.get(i).getCoeff()) + "x^" + Integer.toString(ad.monoame.get(i).getExp());
					else
						if(ad.monoame.get(i).getCoeff()!=0.0)
							adn=adn+Float.toString(ad.monoame.get(i).getCoeff()) + "x^" + Integer.toString(ad.monoame.get(i).getExp())+"+";
		
			}
			adn=adn.replace("+-", "-");
			label.setText(adn);
		}
		else
			if(source == button2){
				Polinom p = new Polinom(s);
				Polinom n = new Polinom(s2);
				Polinom sc=new Polinom();
				sc=sc.scadere(p, n);
				String scd="0";
				for(int i=0;i<sc.monoame.size();i++)
				{
					if(i==0)
						scd=Float.toString(sc.monoame.get(i).getCoeff()) + "x^" + Integer.toString(sc.monoame.get(i).getExp())+"+";
					else
						if(i==sc.monoame.size()-1)
							scd=scd+Float.toString(sc.monoame.get(i).getCoeff()) + "x^" + Integer.toString(sc.monoame.get(i).getExp());
						else
							if(sc.monoame.get(i).getCoeff()!=0.0)
								scd=scd+Float.toString(sc.monoame.get(i).getCoeff()) + "x^" + Integer.toString(sc.monoame.get(i).getExp())+"+";
			
				}
				scd=scd.replace("+-", "-");
				label2.setText(scd);
			}
				else
					if(source == button3){
						Polinom p = new Polinom(s);
						p=p.derivare(p);
						String drv="0";
						for(int i=0;i<p.monoame.size();i++)
						{
							if(i==0)
								drv=Float.toString(p.monoame.get(i).getCoeff()) + "x^" + Integer.toString(p.monoame.get(i).getExp())+"+";
							else
								if(i==p.monoame.size()-1)
									drv=drv+Float.toString(p.monoame.get(i).getCoeff()) + "x^" + Integer.toString(p.monoame.get(i).getExp());
								else
									if(p.monoame.get(i).getCoeff()!=0.0)
										drv=drv+Float.toString(p.monoame.get(i).getCoeff()) + "x^" + Integer.toString(p.monoame.get(i).getExp())+"+";
					
						}
						drv=drv.replace("+-", "-");
						label3.setText(drv);
					}
						else
							if(source == button4){
								Polinom p = new Polinom(s);
								p=p.integrare(p);
								String intr="0";
								for(int i=0;i<p.monoame.size();i++)
								{
									if(i==0)
										intr=Float.toString(p.monoame.get(i).getCoeff()) + "x^" + Integer.toString(p.monoame.get(i).getExp())+"+";
									else
										if(i==p.monoame.size()-1)
											intr=intr+Float.toString(p.monoame.get(i).getCoeff()) + "x^" + Integer.toString(p.monoame.get(i).getExp());
										else
											if(p.monoame.get(i).getCoeff()!=0.0)
												intr=intr+Float.toString(p.monoame.get(i).getCoeff()) + "x^" + Integer.toString(p.monoame.get(i).getExp())+"+";
							
								}
								intr=intr.replace("+-", "-");
								label4.setText(intr);
							}
								else
									if(source == button5){
									Polinom p = new Polinom(s);
									Polinom n = new Polinom(s2);
									Polinom inm=new Polinom();
									inm=inm.inmultire(p, n);
									String inmu="0";
									for(int i=0;i<inm.monoame.size();i++)
									{
										if(i==0)
											inmu=Float.toString(inm.monoame.get(i).getCoeff()) + "x^" + Integer.toString(inm.monoame.get(i).getExp())+"+";
										else
											if(i==inm.monoame.size()-1)
												inmu=inmu+Float.toString(inm.monoame.get(i).getCoeff()) + "x^" + Integer.toString(inm.monoame.get(i).getExp());
											else
												if(inm.monoame.get(i).getCoeff()!=0.0)
													inmu=inmu+Float.toString(inm.monoame.get(i).getCoeff()) + "x^" + Integer.toString(inm.monoame.get(i).getExp())+"+";
								
									}
									inmu=inmu.replace("+-", "-");
									label5.setText(inmu);
									}
	}
}