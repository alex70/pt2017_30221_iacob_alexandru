package polinom;

public class Operatii 
{
	public  Polinom adunare( Polinom X, Polinom Y) 
	{
		for (Monom m : X.monoame)
		{
			int este=0;
			for(Monom n : Y.monoame)
			{	
				if(m.getExp()==n.getExp())
				{
					n.add(m);
					este=1;
				}
			}
			if (este==0)
				Y.monoame.add(m);
		}
	return Y;
	}
	public  Polinom scadere( Polinom X, Polinom Y) 
	{
		for (Monom n : X.monoame)
		{
			int este=0;
			for(Monom m : Y.monoame)
			{	
				if(m.getExp()==n.getExp())
				{
					m.substract(n);
					este=1;
				}
			}
			if (este==0)
				Y.monoame.add(new Monom(n.getExp(),-n.getCoeff()));
				
		}
		return Y;
	}
	public  Polinom derivare(Polinom X) 
	{
		for (Monom m : X.monoame)
		{
			m.deriv();
		}
		return X;
	}
	public  Polinom integrare(Polinom X) 
	{
		for (Monom m : X.monoame)
		{
			m.integr();
		}
		return X;
	}
	public Polinom inmultire(Polinom X, Polinom Y)
	{
		Polinom P=new Polinom();
		for(Monom m: X.monoame)
		{
			Polinom Pp=new Polinom();
			for(Monom n: Y.monoame)
			{
				Monom g=new Monom(m.getExp(),m.getCoeff());
				Pp.monoame.add(g.inmultire(n));
			}
			P=P.adunare(P, Pp);
		}
		return P;
	}
}