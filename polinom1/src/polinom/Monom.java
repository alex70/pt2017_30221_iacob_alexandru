package polinom;
 
public class Monom {
	private int exp;
	private float coeff;
	public Monom()
	{
		exp=0;
		coeff=0;
	}
	public Monom(String s)
	{
		int i,este=1;
		for(i=0;i<s.length();i++)
		{
			este=0;
			if(s.charAt(i) == 'x')
			{
				if((i==1) && (s.charAt(0) =='-'))
				{
					coeff=-1;
					if(i+2<=s.length())
						exp=Integer.parseInt(s.substring(i+2,s.length()));
					else
						exp=1;
				}
				else
					if(i==0)
						coeff=1;
					else
						coeff = Float.parseFloat(s.substring(0, i));
					if(i==s.length()-1)
						exp=1;
					else
						exp = Integer.parseInt(s.substring(i+2,s.length()));
					return;
			}
		}
		if(este==0)
		{
			coeff=Integer.parseInt(s);
			exp=0;
		}
	}
	public Monom(int exp, float f)
	{
		this.exp = exp;
		this.coeff = f;
	}
	public int getExp()
	{
		return exp;
	}
	public float getCoeff()
	{
		return coeff;
	}
	public void add(Monom a)
	{
		this.coeff+=a.coeff;
	}
	public void substract(Monom m)
	{
			this.coeff = this.coeff-m.coeff ;
	}
	public void deriv()
	{
		coeff = coeff * exp;
		exp--;
	}
	public void integr()
	{
		exp++;
		coeff = coeff / exp;
	}
	public Monom inmultire(Monom m)
	{
		Monom product = new Monom();
		product.coeff = this.coeff * m.coeff;
		product.exp = this.exp + m.exp;
		return product;
	}
	public void print()
	{
			System.out.println(coeff+"x^"+exp);
	}
	
}