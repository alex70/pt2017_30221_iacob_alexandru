package polinom;
import java.util.ArrayList;

public class Polinom extends Operatii {
	ArrayList<Monom> monoame=new ArrayList<Monom>();
	public Polinom()
	{
		
	}
	public Polinom(String s)
	{
		s=s.replace("-", "+-");
		String[] splits = s.split("[+]");
		int i;
		for(i=0;i<splits.length;i++)
				monoame.add(new Monom(splits[i]));	
	}
	public void print()
	{
		for(Monom m : monoame)
			m.print();
	}
}
